#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

def main(nr):
  import datetime
  import os
  import subprocess
  import time

  if os.path.isfile("/home/matthias/content"):
    lfile = open("/home/matthias/smslogfile.log", "a")

    hfile = open("/home/matthias/heizungsStufe")
    stufe = hfile.read()
    hfile.close()
    sfile = open("/home/matthias/restsms")
    sms = sfile.read()
    sfile.close()
    smscount = int(sms)
    smscount -= 1
    sms = str(smscount)

    cfile = open("/home/matthias/content")
    text = cfile.read()
    cfile.close()

    output = text + " r: " + sms + " s: " + stufe
    if (len(nr) == 0) :
      nr = "+4917651012493"

# echo "my message" | gammu-smsd-inject TEXT "+491234..."
    command = "echo " + "\"" + output
    command = command + "\" | gammu-smsd-inject TEXT \"" + nr + "\""
#    print(command)
    sfile = open("/home/matthias/restsms", "w")
    sfile.write(sms)
    sfile.close()
    time.sleep(60)
    os.system(command)
#temp = subprocess.check_output(command, shell=True)
    lfile.write("sendSMS.py to " + nr + "\n")
    lfile.close
    os.system("rm /home/matthias/content")

if __name__ == "__main__":
  if (len(sys.argv) < 2):
    main("")
  else:
    main(sys.argv[1])

