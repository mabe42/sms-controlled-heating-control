# sms-controlled-heating-control

The whole control ssystem is based on the software [gammu](https://github.com/gammu/gammu). This handles receiving and sending of text messages using the UMTS-Stick. A stepper motor (28BYJ-48) drives a belt to turn the button of the heater. 

The configuration of gammu can be found in **/etc/gammu-smsdrc**. Whenever a text message is received the script **receivedSMS.py** is executed.

Cron jobs (user level) measure every hour the temperature in the room, calculate daily statistical values (average, minimum, maximum) and send the values of the previous weekend to my phone each Monday morning.

From **crontab -l**:
>   # m h  dom mon dow   command  
> 3 * * * * /home/matthias/getTemperature.py  
> 5 23 * * * /home/matthias/calcAvg.py  
> 0 7 * * mon /home/matthias/sendWeekend.py  
> */2 * * * * /home/matthias/sendSMS.py  

## getTemperature.py

The temperature is measured each hour using a DS1820 temperature sensor which is connected to the 1-wire bus of the raspi. These values are written to the daily log files (~/mon.log). These log files are overwritten each week.

## calcAvg.py

Every evening the average, min and max temperatures of the day are calculated from the values of the daily log file. They are written to daily files (~/mon.avg, ...)

## receivedSMS.py

Whenever a text message is received, this script will be executed. Depending on the key word in the message different actions are triggered. If none of these key words is recognized, the text message is forwarded to my phone. Since this script cannot send text messages, a workaround for sending messages is necessary: the text of the message to be sent is written to **~/content**. Every two minutes **sendSMS.py** checks if this file is present and accordingly sends the content as text message to my phone.

Depending on the key word the following actions are triggered:
* Reboot: rebooting the raspi
* Status: a status message is sent as text message to my phone (**~/sendStatus.py**)
* Restsms: After the SIM card has been recharged, the newly calculated number of text messages is transferred to the raspi. This number is stored in **~/restsms**.
* Stufe: Setting the heater to a value between 0 and 10 (calling **~/turnSteps.py**). This value is saved to **~/heizungsStufe**.

## sendSMS.py

If **~/content** is present, the content is sent to my phone. Afterwards **~/content** is deleted.

## sendStatus.py

Present temperature, present setting of the heater, number of remaining text messages, uptime, date and time are written to **~/content** such that all this information is sent the next time **~/sendSMS** is executed.

## turnSteps.py

This handles the stepper motor. I took the code from <http://www.elektronx.de/tutorials/schrittmotorsteuerung-mit-dem-raspberry-pi/>.

## sendWeekend.py

On Monday mornings the temperatures of the previous three days, the present setting of the heater and the remaining number of text messages are written to **~/content** and eventually sent by **~/sendSMS.py**.

