#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import os

DayL = ['fri','sat','sun']
tfile = open("/sys/bus/w1/devices/10-000802e75cf1/w1_slave")
text = tfile.read()
tfile.close()

temperature_data = text.split()[-1]
temperature = float(temperature_data[2:])
temperature = temperature / 1000
#print(temperature)

output = ""
for day in DayL:
  avgname = day+".avg"
  avgfile = open(avgname, "r")
  dayavg = avgfile.read()
#  print(dayavg)
  avgfile.close()
  output += dayavg

output = output + "now " + '{:03.1f}'.format(temperature) 
cfile = open("/home/matthias/content", "w")
cfile.write(output)
cfile.close()
# print(output)
# echo "test2f" | gammu-smsd-inject TEXT "+491234..."
#command = "echo "
#command = command + output + " | gammu-smsd-inject TEXT \"+491234...\""
# print(command)
#os.system(command)
lfile = open("/home/matthias/smslogfile.log", "a")
lfile.write("sendWeekend.py wrote content\n")
lfile.close
